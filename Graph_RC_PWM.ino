/*
  Graph RC PWM
  Read PWM values from input pins and print them to serial in a comma seperated format.

  2021 Raynition
*/

// initialize pins defined in an array
int pwmPins[] = {
 9, 10, 11
};
int pinCount = sizeof(pwmPins) / sizeof(int);


void setup() {
  // initialize the serial communication:
  Serial.begin(9600);
  for (int thisPin = 0; thisPin < pinCount; thisPin++) {
    pinMode(pwmPins[thisPin], INPUT);
  }
}

int readChan(int pin) {
  int reading = pulseIn(pin, HIGH);
  return reading;
//  return map(reading, 1000, 2000, -100, 100);
}
void loop() {
  for (int thisPin = 0; thisPin < pinCount; thisPin++) {
    Serial.print(readChan(pwmPins[thisPin]));
    if(thisPin != pinCount-1) {
    // This is NOT the Last pin.
    // Add a , to seperate values.
    Serial.print(",");
    }
  }
  // We've gone through all the pins. print a new line character and continue.
  Serial.println();
  // limit speed of readings.
  delay(5);
}
